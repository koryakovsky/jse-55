package ru.nlmk.study.jse55.ex1;

import org.hibernate.SessionFactory;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import ru.nlmk.study.jse55.Car;

/**
 * Конфигурация hibernate через xml
 */
public class HibernateConfig1 {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        final StandardServiceRegistry registry = new StandardServiceRegistryBuilder()
                .configure() // ищет конфигурацию в файле hibernate.cfg.xml
                .build();

        sessionFactory = new MetadataSources(registry)
                .addAnnotatedClass(Car.class)
                .buildMetadata().buildSessionFactory();

        return sessionFactory;
    }
}
