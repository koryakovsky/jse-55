package ru.nlmk.study.jse55.ex3;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import ru.nlmk.study.jse55.Car;

/**
 * Data access object
 */
public class CarDao {

    private final SessionFactory sessionFactory = HibernateConfig3.getSessionFactory();

    public void save(Car car) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.save(car);

        tx.commit();
        session.close();
    }

    public Car getById(Long id) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        Car car = session.get(Car.class, id);

        tx.commit();
        session.close();

        return car;
    }

    public void delete(Car car) {
        Session session = sessionFactory.openSession();
        Transaction tx = session.beginTransaction();

        session.delete(car);

        tx.commit();
        session.close();
    }
}
