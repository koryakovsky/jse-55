package ru.nlmk.study.jse55.ex3;

import ru.nlmk.study.jse55.Car;

public class App3 {
    public static void main(String[] args) {
        CarDao carDao = new CarDao();

        Car car = new Car();
        car.setId(1L);
        car.setVin("223434HGHAG");
        car.setModel("Audi");
        car.setReleaseDate("02-04-2018");

        carDao.save(car);


        Car savedCar = carDao.getById(1L);

        System.out.println(savedCar);

        carDao.delete(savedCar);

        savedCar = carDao.getById(1L);

        System.out.println(savedCar);
    }
}
