package ru.nlmk.study.jse55.ex3;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.service.ServiceRegistry;
import ru.nlmk.study.jse55.Car;

public class HibernateConfig3 {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        Configuration cfx = new Configuration();
        cfx.addAnnotatedClass(Car.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfx.getProperties())
                .build();

        sessionFactory = cfx.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
