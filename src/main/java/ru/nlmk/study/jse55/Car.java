package ru.nlmk.study.jse55;

import javax.persistence.*;

@Entity
@Table(name = "car")
public class Car {

    @Id
    @Column // если поле и колонка совпадают -> @Column не обязательно
    private Long id;

    @Column(length = 15)
    private String vin;

    @Column
    private String model;

    @Column(name = "release_date")
    private String releaseDate;

    public void setId(Long id) {
        this.id = id;
    }

    public Long getId() {
        return id;
    }

    public String getVin() {
        return vin;
    }

    public void setVin(String vin) {
        this.vin = vin;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getReleaseDate() {
        return releaseDate;
    }

    public void setReleaseDate(String releaseDate) {
        this.releaseDate = releaseDate;
    }

    @Override
    public String toString() {
        return "Car{" +
                "id=" + id +
                ", vin='" + vin + '\'' +
                ", model='" + model + '\'' +
                ", releaseDate='" + releaseDate + '\'' +
                '}';
    }
}
