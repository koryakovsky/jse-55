package ru.nlmk.study.jse55.ex4;

import ru.nlmk.study.jse55.Car;

public class App4 {

    public static void main(String[] args) {
        CarDao2 carDao = new CarDao2();

        Car car = new Car();
        car.setId(3L);
        car.setModel("Lada");
        car.setVin("2342394HD");
        car.setReleaseDate("01-01-1970");

        carDao.save(car);
    }
}
