package ru.nlmk.study.jse55.ex4;

import ru.nlmk.study.jse55.Car;

import javax.persistence.EntityManager;

import static ru.nlmk.study.jse55.ex4.PersistenceConfig.getEntityManager;

public class CarDao2 {

    public void save(Car car) {
        EntityManager em = getEntityManager();
        em.getTransaction().begin();

        em.persist(car);

        em.getTransaction().commit();
        em.close();
    }
}
