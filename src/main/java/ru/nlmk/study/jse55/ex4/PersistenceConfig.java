package ru.nlmk.study.jse55.ex4;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

public class PersistenceConfig {

    /**
     *  EntityManagerFactory аналог SessionFactory
     */
    private static final EntityManagerFactory emf =
            Persistence.createEntityManagerFactory("JSE-55-em");

    /**
     * EntityManager аналог Session
     * @return EntityManager
     */
    public static EntityManager getEntityManager() {
        return emf.createEntityManager();
    }
}
