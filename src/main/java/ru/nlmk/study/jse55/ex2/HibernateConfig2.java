package ru.nlmk.study.jse55.ex2;

import org.hibernate.SessionFactory;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.hibernate.cfg.Configuration;
import org.hibernate.cfg.Environment;
import org.hibernate.service.ServiceRegistry;
import ru.nlmk.study.jse55.Car;

import java.util.Properties;

/**
 * Java based config
 */
public class HibernateConfig2 {

    private static SessionFactory sessionFactory;

    public static SessionFactory getSessionFactory() {
        if (sessionFactory != null) return sessionFactory;

        Configuration cfx = new Configuration();

        Properties props = new Properties();
        props.put(Environment.DRIVER, "org.h2.Driver");
        props.put(Environment.URL, "jdbc:h2:mem:test");
        props.put(Environment.USER, "sa");
        props.put(Environment.PASS, "");
        props.put(Environment.DIALECT, "org.hibernate.dialect.H2Dialect");
        props.put(Environment.SHOW_SQL, "true");
        props.put(Environment.HBM2DDL_AUTO, "create-drop");

        cfx.setProperties(props);
        cfx.addAnnotatedClass(Car.class);

        ServiceRegistry serviceRegistry = new StandardServiceRegistryBuilder()
                .applySettings(cfx.getProperties())
                .build();

        sessionFactory = cfx.buildSessionFactory(serviceRegistry);

        return sessionFactory;
    }
}
